/*
 * Copyright 2001, Trustees of Boston University.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Author:     Alberto Medina
 *              Anukool Lakhina
 *  Title:     BRITE: Boston university Representative Topology gEnerator
 *  Revision:  2.0         4/02/2001
 */
/****************************************************************************/
/*                                                                          */
/*  Modified lightly to easily interface with ns-3                          */
/*  Author:     Josh Pelkey <jpelkey@gatech.edu>                            */
/*  Date: 3/02/2011                                                         */
/****************************************************************************/
/*  Relicensed to GPLv2, with permission of the original authors.           */
/*  Date: 04/20/2024                                                        */
/****************************************************************************/

#ifndef BU_MODEL_H
#define BU_MODEL_H

#include "Model.h" 

namespace brite {

//////////////////////////////////////////////
//
// class BottomUpHierModel
// Derived class for hierarchical models that
// build topologies following a bottom-down approach
//
//////////////////////////////////////////////

class BottUpPar;

class BottomUpHierModel : public Model {
    
 public:
    
  BottomUpHierModel(BottUpPar* par);
  Graph* Generate(); 
  void GroupNodes(Graph* g);
  void SetModel(Model* m, int i) { assert(m != NULL); models[i] = m; }
  void RandomWalk(Graph* g, std::vector<Color>&, int last, int size, RandomVariable& U, int as, int& c);
  void AssignBW(Graph* g);
  int GetBWInterDist() { return BWInterdist; }
  double GetBWInterMin() { return BWIntermin; }
  double GetBWInterMax() { return BWIntermax; }
  std::string ToString();
    
 private:
    
  int nlevels;
  int ASNodes;
  std::vector<Model*> models;
  GroupingType group;
  AssignmentType at;
  BWDistType BWInterdist;
  double BWIntermin;
  double BWIntermax;

};

} // namespace brite

#endif /* MODEL_H */
