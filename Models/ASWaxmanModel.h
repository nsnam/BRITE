/*
 * Copyright 2001, Trustees of Boston University.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Author:     Alberto Medina
 *              Anukool Lakhina
 *  Title:     BRITE: Boston university Representative Topology gEnerator
 *  Revision:  2.0         4/02/2001
 */
/****************************************************************************/
/*                                                                          */
/*  Modified lightly to easily interface with ns-3                          */
/*  Author:     Josh Pelkey <jpelkey@gatech.edu>                            */
/*  Date: 3/02/2011                                                         */
/****************************************************************************/
/*  Relicensed to GPLv2, with permission of the original authors.           */
/*  Date: 04/20/2024                                                        */
/****************************************************************************/

#ifndef AS_WAX_MODEL_H
#define AS_WAX_MODEL_H

#include "ASModel.h"

namespace brite {

//////////////////////////////////////////////
//
// class ASWaxman
// Derived class for Waxman model. This model
// implies no incremental growth and Waxman's
// connectivity probability function.
// Builds AS-level topologies 
//
//////////////////////////////////////////////

class ASWaxPar;

class ASWaxman : public ASModel {
    
 public:
    
  ASWaxman(ASWaxPar* par);
  Graph* Generate();
  std::string ToString();

 protected:
  void InterconnectNodes(Graph* graph);
  double ProbFunc(BriteNode* src, BriteNode* dst);
  
 private:
  double alpha;
  double beta;

};

} // namespace brite

#endif /* AS_WAX_MODEL_H */
