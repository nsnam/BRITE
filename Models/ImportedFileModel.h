/*
 * Copyright 2001, Trustees of Boston University.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Author:     Alberto Medina
 *              Anukool Lakhina
 *  Title:     BRITE: Boston university Representative Topology gEnerator
 *  Revision:  2.0         4/02/2001
 */
/****************************************************************************/
/*                                                                          */
/*  Modified lightly to easily interface with ns-3                          */
/*  Author:     Josh Pelkey <jpelkey@gatech.edu>                            */
/*  Date: 3/02/2011                                                         */
/****************************************************************************/
/*  Relicensed to GPLv2, with permission of the original authors.           */
/*  Date: 04/20/2024                                                        */
/****************************************************************************/

#ifndef IF_MODEL_H

#define IF_MODEL_H

#include "Model.h"

namespace brite {

////////////////////////////////////////
//
// class ImportedFileModel
// Base class for models that generate 
// topologies imported from files containing
// Previously generated topologies
//
////////////////////////////////////////

class ImportedFilePar;

class ImportedFileModel : public Model {

 public:
  
  enum ImportedFileFormat { IF_BRITE = 1, IF_GTITM = 2, 
			    IF_NLANR = 3, IF_SKITTER = 4, 
			    IF_GTITM_TS = 5, IF_INET = 6 };
  enum Level {RT_LEVEL = 1, AS_LEVEL = 2 };
  ImportedFileModel(ImportedFilePar* par);
  virtual Graph* Generate() { return (Graph*)NULL; }
  ImportedFileFormat GetFileFormat() { return format; }
  int GetBW() { return BWdist; }
  double GetBWMin() { return BWmin; }
  double GetBWMax() { return BWmax; }
  std::string ToString();
  void AssignBW(Graph* g);
  BWDistType GetBWDist() { return BWdist; }
  void SetBWDist(BWDistType t) { BWdist = t; }
  void SetBWMin(double bw) { BWmin = bw; }
  void SetBWMax(double bw) { BWmax = bw; }

 protected:
  ImportedFileFormat format;
  std::vector<std::string> model_strings;
  Level level;
  int num_strings;
  std::string filename;

 private:
  BWDistType BWdist;
  double BWmin;
  double BWmax;

};


class ImportedBriteTopologyModel : public ImportedFileModel {

 public:
  ImportedBriteTopologyModel(ImportedFilePar* par);
  Graph* Generate(); 

 private:
  Graph* ParseFile();
  
};

class ImportedGTitmTopologyModel : public ImportedFileModel {

 public:
  ImportedGTitmTopologyModel(ImportedFilePar* par);
  Graph* Generate(); 

 private:
  Graph* ParseFile();
  Graph* ParseFlatGTITM();
  Graph* ParseTSGTITM();

};


class ImportedNLANRTopologyModel : public ImportedFileModel {
 public:
  ImportedNLANRTopologyModel(ImportedFilePar* par);
  Graph* Generate(); 

 private:
  Graph* ParseFile();
  void PlaceNode(Graph*, int, std::string);
  void PlaceEdge(Graph*, int, int);
  RandomVariable U;

};

class ImportedInetTopologyModel : public ImportedFileModel {

 public:
  ImportedInetTopologyModel(ImportedFilePar* par);
  Graph* Generate(); 

 private:
  Graph* ParseFile();
  
};

class ImportedSkitterTopologyModel : public ImportedFileModel {

 public:
  ImportedSkitterTopologyModel(ImportedFilePar* par);
  Graph* Generate();
  int GetNumStrings() { return num_strings; }

 private:
  Graph* ParseFile();

};

} // namespace brite

#endif /* IF_MODEL_H */
