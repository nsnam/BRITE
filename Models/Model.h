/*
 * Copyright 2001, Trustees of Boston University.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Author:     Alberto Medina
 *              Anukool Lakhina
 *  Title:     BRITE: Boston university Representative Topology gEnerator
 *  Revision:  2.0         4/02/2001
 */
/****************************************************************************/
/*                                                                          */
/*  Modified lightly to easily interface with ns-3                          */
/*  Author:     Josh Pelkey <jpelkey@gatech.edu>                            */
/*  Date: 3/02/2011                                                         */
/****************************************************************************/
/*  Relicensed to GPLv2, with permission of the original authors.           */
/*  Date: 04/20/2024                                                        */
/****************************************************************************/

#ifndef MODEL_H
#define MODEL_H

#include "../Graph.h"
#include "../Parser.h"
#include <algorithm>

namespace brite {

enum PlacementType { P_RANDOM = 1, P_HT = 2 };
enum GrowthType { G_INCR = 1, G_ALL = 2 };
enum PrefType { PC_NONE = 1, PC_BARABASI = 2 };
enum ConnLocType { CL_ON = 1, CL_OFF = 2 };
enum BWDistType { BW_CONST = 1, BW_UNIF = 2, BW_EXP = 3, BW_HT = 4};
enum DelayDistType { D_DISTANCE = 1, D_TECH = 2};
enum ModelType {RT_WAXMAN = 1, RT_BARABASI = 2, 
		 AS_WAXMAN = 3, AS_BARABASI = 4,
		 TD_HIER = 5, BU_HIER = 6, IF_ROUTER = 7, IF_AS = 8};
enum EdgeConnType { TD_RANDOM = 1, TD_SMALLEST = 2, 
		    TD_SMALLEST_NOLEAF = 3, TD_K_DEGREE = 4};
enum GroupingType { BU_RANDOM_PICK = 1, BU_RANDOM_WALK = 2 };
enum AssignmentType { A_CONST = 1, A_UNIF = 2, A_EXP = 3, A_HT = 4 };

class Graph;
class BriteNode;

//////////////////////////////////////////////
//
// class Model
// Base class for all generation models
//
//////////////////////////////////////////////

class PlaneRowAdjNode {

 public:
  PlaneRowAdjNode(int tx) { x = tx; }
  int GetX() { return x; }
  bool ColFind(int ty);
  void ColInsert(int ty);
  
 private:
  int x;
  std::list<int> row_adjlist;

};

class Model {
  
  friend class RandomVariable;

 public:

  Model() {};
  virtual ~Model() {};

  virtual Graph* Generate() {return (Graph*)NULL;}
  void PlaceNodes(Graph* g);
  int GetPlacementType() { return NodePlacement; }
  int GetGrowthType() { return Growth; }
  int GetPrefType() { return PrefConn; }
  int GetConnLocType() { return ConnLoc; }
  int GetSize() { return size; }
  ModelType GetType() { return type; }
  int GetMEdges() { return m_edges; }
  std::string ToString();
  bool PlaneCollision(int tx, int ty);
  
  /* Random Variable seeds */
  static unsigned short int s_places[3];
  static unsigned short int s_connect[3];
  static unsigned short int s_edgeconn[3];
  static unsigned short int s_grouping[3];
  static unsigned short int s_assignment[3];
  static unsigned short int s_bandwidth[3];

 protected:

  PlacementType NodePlacement;
  GrowthType Growth;
  PrefType PrefConn;
  ConnLocType ConnLoc;
  ModelType type;
  int Scale_1;
  int Scale_2;
  int m_edges;
  int size;
  static std::vector<PlaneRowAdjNode*> row_ocup;

};

} // namespace brite

#endif /* MODEL_H */
