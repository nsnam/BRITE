/*
 * Copyright 2001, Trustees of Boston University.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Author:     Alberto Medina
 *              Anukool Lakhina
 *  Title:     BRITE: Boston university Representative Topology gEnerator
 *  Revision:  2.0         4/02/2001
 */
/****************************************************************************/
/*                                                                          */
/*  Modified lightly to easily interface with ns-3                          */
/*  Author:     Josh Pelkey <jpelkey@gatech.edu>                            */
/*  Date: 3/02/2011                                                         */
/****************************************************************************/
/*  Relicensed to GPLv2, with permission of the original authors.           */
/*  Date: 04/20/2024                                                        */
/****************************************************************************/

#ifndef TD_MODEL_H
#define TD_MODEL_H

#include "Model.h"

namespace brite {

//////////////////////////////////////////////////
//
// class TopDownHierModel
// Derived class for hierarchical models that
// build topologies following a top-down approach
//
///////////////////////////////////////////////////

class TopDownPar;

class TopDownHierModel : public Model {

 public:

  TopDownHierModel(TopDownPar* par);

  int GetK() { return k; }
  int GetLevels() { return nlevels; }
  void SetModel(Model* m, int i) { assert(m != NULL); models[i] = m; }
  EdgeConnType GetEdgeConnType() { return edge_conn_type; }
  Graph* Generate(); 
  Graph* FlattenGraph(Graph* g);
  void InterConnectBorders(Graph* g, Graph* flat_g);
  int GetFlatRandomNode(int as, Graph* g, Graph* flat_g, std::vector<int>& p, RandomVariable& U);
  int GetFlatSmallest(int, Graph*, Graph*, std::vector<int>& p); 
  int GetFlatSmallestNoLeaf(int, Graph*, Graph*, std::vector<int>& p);
  int GetFlatSmallestK(int ASid, Graph* g, Graph* flat_g, std::vector<int>& p);
  int GetBWInterDist() { return BWInterdist; }
  double GetBWInterMin() { return BWIntermin; }
  double GetBWInterMax() { return BWIntermax; }
  int GetBWIntraDist() { return BWIntradist; }
  double GetBWIntraMin() { return BWIntramin; }
  double GetBWIntraMax() { return BWIntramax; }
  std::string ToString();

 private:
    
  int nlevels;
  std::vector<Model*> models;
  int k;
  EdgeConnType edge_conn_type;
  BWDistType BWInterdist;
  double BWIntermin;
  double BWIntermax;
  BWDistType BWIntradist;
  double BWIntramin;
  double BWIntramax;
  
};

} // namespace brite

#endif /* TD_MODEL_H */
