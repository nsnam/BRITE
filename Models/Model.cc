/*
 * Copyright 2001, Trustees of Boston University.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Author:     Alberto Medina
 *              Anukool Lakhina
 *  Title:     BRITE: Boston university Representative Topology gEnerator
 *  Revision:  2.0         4/02/2001
 */
/****************************************************************************/
/*                                                                          */
/*  Modified lightly to easily interface with ns-3                          */
/*  Author:     Josh Pelkey <jpelkey@gatech.edu>                            */
/*  Date: 3/02/2011                                                         */
/****************************************************************************/
/*  Relicensed to GPLv2, with permission of the original authors.           */
/*  Date: 04/20/2024                                                        */
/****************************************************************************/

#include "Model.h"

using namespace std;
namespace brite {

unsigned short int Model::s_places[3] = {0,0,0};
unsigned short int Model::s_connect[3] = {0,0,0};
unsigned short int Model::s_edgeconn[3] = {0,0,0};
unsigned short int Model::s_grouping[3] = {0,0,0};
unsigned short int Model::s_assignment[3] = {0,0,0};
unsigned short int Model::s_bandwidth[3] = {0,0,0};
vector<PlaneRowAdjNode*> Model::row_ocup(10000);

void PlaneRowAdjNode::ColInsert(int ty) {
  
  row_adjlist.insert(row_adjlist.end(), ty);
  
};

bool PlaneRowAdjNode::ColFind(int ty) {
  
  list<int>::iterator cl;

  cl = find(row_adjlist.begin(), row_adjlist.end(), ty);
  if (cl == row_adjlist.end()) {

    ColInsert(ty);
    return false;

  }

  return true;

}


bool Model::PlaneCollision(int tx, int ty) {

  bool found = false;

  if (tx >= (int)row_ocup.size()) { 

    row_ocup.resize(tx + 1);

  }else {

    if (row_ocup[tx] != NULL) {

      found = true;

    }

  }

  if (!found) {

    PlaneRowAdjNode* rowadjnode = new PlaneRowAdjNode(tx);
    rowadjnode->ColInsert(ty);
    row_ocup[tx] = rowadjnode;
    return false;
  }
    
  return row_ocup[tx]->ColFind(ty);

}

} // namespace brite
