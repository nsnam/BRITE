/*
 * Copyright 2001, Trustees of Boston University.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Author:     Alberto Medina
 *              Anukool Lakhina
 *  Title:     BRITE: Boston university Representative Topology gEnerator
 *  Revision:  2.0         4/02/2001
 */
/****************************************************************************/
/*                                                                          */
/*  Modified lightly to easily interface with ns-3                          */
/*  Author:     Josh Pelkey <jpelkey@gatech.edu>                            */
/*  Date: 3/02/2011                                                         */
/****************************************************************************/
/*  Relicensed to GPLv2, with permission of the original authors.           */
/*  Date: 04/20/2024                                                        */
/****************************************************************************/

#ifndef ROUTER_MODEL_H
#define ROUTER_MODEL_H

#include "Model.h"

namespace brite {

////////////////////////////////////////
//
// class RouterModel
// Base class for models that generate 
// router-level topologies
//
////////////////////////////////////////

class RouterModel : public Model {

 public:

  ~RouterModel() {}
  virtual Graph* Generate() {return (Graph*)NULL;}  
  void PlaceNodes(Graph* g);
  void AssignBW(Graph* g);

  BWDistType GetBWDistType() { return BWdist; }
  void SetBWDist(BWDistType t) { BWdist = t; }
  void SetBWMin(double bw) { BWmin = bw; }
  void SetBWMax(double bw) { BWmax = bw; }
  int GetBWDist() { return BWdist; }
  double GetBWMin() { return BWmin; }
  double GetBWMax() { return BWmax; }

 private:

  BWDistType BWdist;
  double BWmin;
  double BWmax;

};

} // namespace brite

#endif /* ROUTER_MODEL_H */
