/*
 * Copyright 2001, Trustees of Boston University.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Author:     Alberto Medina
 *              Anukool Lakhina
 *  Title:     BRITE: Boston university Representative Topology gEnerator
 *  Revision:  2.0         4/02/2001
 */
/****************************************************************************/
/*  Modified lightly to easily interface with ns-3                          */
/*  Author:     Josh Pelkey <jpelkey@gatech.edu>                            */
/*  Date: 3/02/2011                                                         */
/****************************************************************************/
/*  Relicensed to GPLv2, with permission of the original authors.           */
/*  Date: 04/20/2024                                                        */
/****************************************************************************/

#include "BriteNode.h"

namespace brite {

BriteNode::BriteNode(int i) {

  nodeId = i;
  nodeAddr = 0;
  inDegree = 0;
  outDegree = 0;
  nodeColor = BLACK;

}

BriteNode::BriteNode(NodeConf* c) {

  nodeInfo = c;

}

ASNodeConf::ASNodeConf() { 

  SetCost(1.0);
  t = NULL;
  SetNodeType(AS_NODE);
  astype = AS_NONE;
  
}

RouterNodeConf::RouterNodeConf() { 

  SetCost(1.0);
  SetNodeType(RT_NODE);
  rttype = RT_NONE;

}


void ASNodeConf::SetTopology(Topology* top, int asid) {
  
  t = top; 
  if (t != NULL) {
    Graph* g = t->GetGraph();
    for (int i = 0; i < g->GetNumNodes(); i++) {
      RouterNodeConf* rt_conf = (RouterNodeConf*)(g->GetNodePtr(i)->GetNodeInfo());
      rt_conf->SetASId(asid);
    }
  } 
}

} // namespace brite
