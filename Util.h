/*
 * Copyright 2001, Trustees of Boston University.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Author:     Alberto Medina
 *              Anukool Lakhina
 *  Title:     BRITE: Boston university Representative Topology gEnerator
 *  Revision:  2.0         4/02/2001
 */
/****************************************************************************/
/*  Modified lightly to easily interface with ns-3                          */
/*  Author:     Josh Pelkey <jpelkey@gatech.edu>                            */
/*  Date: 3/02/2011                                                         */
/****************************************************************************/
/*  Relicensed to GPLv2, with permission of the original authors.           */
/*  Date: 04/20/2024                                                        */
/****************************************************************************/

#ifndef UTIL_H
#define UTIL_H

#include <iostream>
#include <sstream>
#include <string>
#include <math.h>
#include <fstream>
#include <new>
#include <vector>
#include <list>
#include <assert.h>
#include <cstdint>
#include <stdlib.h>

#ifdef __WIN32__
double erand48(unsigned short xseed[3]);
#endif

namespace brite {

#define MAXNUM 0x3FFFFFFF
#define	MAXINT	2147483647

enum Color { YELLOW, BLUE, RED, WHITE,
	     BLACK, PURPLE, CYAN, PINK, GRAY};

enum Seed {PLACES = 1, CONNECT = 2};


class RandomVariable {

 public:

  RandomVariable(unsigned short int* seed);
  ~RandomVariable();
  double GetValUniform();
  double GetValUniform(double r);
  double GetValUniform(double a, double b);
  double GetValExponential(double lambda);
  double GetValNormal(double avg, double std);
  double GetValPareto(double scale, double shape);
  double GetValLognormal(double avg, double std);
  unsigned short int GetSeed(int i) {return seed[i];}

 private:

  unsigned short int seed[3];
  unsigned short int* sptr;

};

inline double RandomVariable::GetValUniform() {

  return erand48(seed);

}

inline double RandomVariable::GetValUniform(double r) {

  return  r * erand48(seed);

}

inline double RandomVariable::GetValUniform(double min, double max) {
  return min + GetValUniform(max - min);
}

inline double RandomVariable::GetValExponential(double lambda) {
  assert(lambda > 0);
  return (-log(GetValUniform())/lambda);  
}

inline double RandomVariable::GetValPareto(double scale, double shape) {

  assert(shape > 0);
  double x = GetValUniform();
  double den = pow(1.0 - x + x*pow(1.0/scale, shape), 1.0/shape);
  double res = 1.0/den;
  return res;

}

inline double RandomVariable::GetValLognormal(double avg, double std) {
  
  return (exp(GetValNormal(avg, std))); 

}

int BinarySearch(std::vector<double>&, int, int, double);

} // namespace brite

#endif /* UTIL_H */
