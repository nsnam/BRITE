/*
 * Copyright 2001, Trustees of Boston University.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Author:     Alberto Medina
 *              Anukool Lakhina
 *  Title:     BRITE: Boston university Representative Topology gEnerator
 *  Revision:  2.0         4/02/2001
 */
/****************************************************************************/
/*                                                                          */
/*  Modified lightly to easily interface with ns-3                          */
/*  Author:     Josh Pelkey <jpelkey@gatech.edu>                            */
/*  Date: 3/02/2011                                                         */
/****************************************************************************/
/*  Relicensed to GPLv2, with permission of the original authors.           */
/*  Date: 04/20/2024                                                        */
/****************************************************************************/

#ifndef GRAPH_H
#define GRAPH_H

#include "BriteNode.h"
#include "Edge.h"

namespace brite {

class BriteNode;
class Edge;

class Graph {

  friend class Topology;
  friend class BottomUpHierModel;
  friend class TopDownHierModel;
  friend class RouterModel;
  friend class ASModel;
  friend class ImportedFileModel;
  friend class Analysis;

 public:
  
  Graph(int size);

  void AddNode(BriteNode* node, int i);
  void AddEdge(Edge* edge);
  void AddAdjListNode(int n1, int n2);
  void AddIncListNode(Edge* edge);
  BriteNode* GetNodePtr(int index);
  int GetAdjListSize(int u) { return adjList[u].size(); }
  int GetIncListSize(int u) { return incList[u].size(); }
  int GetEdgeListSize() { return edges.size(); }
  int GetNumNodes();
  int GetNumEdges();
  std::list<Edge*> GetEdges (void) { return edges; }
  void SetNumNodes(int n);
  bool AdjListFind(int n1, int n2);
  void DFS(std::vector<Color>&, std::vector<int>&, int u);

 private:

  int numNodes;
  int numEdges;
  std::vector<BriteNode*> nodes;
  std::list<Edge*> edges;
  std::vector< std::list<int> > adjList;
  std::vector< std::list<Edge*> > incList;

};

} // namespace brite

#endif /* GRAPH_H */
