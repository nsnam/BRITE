/*
 * Copyright 2001, Trustees of Boston University.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Author:     Alberto Medina
 *              Anukool Lakhina
 *  Title:     BRITE: Boston university Representative Topology gEnerator
 *  Revision:  2.0         4/02/2001
 */
/****************************************************************************/
/*                                                                          */
/*  Modified lightly to easily interface with ns-3                          */
/*  Author:     Josh Pelkey <jpelkey@gatech.edu>                            */
/*  Date: 3/02/2011                                                         */
/****************************************************************************/
/*  Relicensed to GPLv2, with permission of the original authors.           */
/*  Date: 04/20/2024                                                        */
/****************************************************************************/

#include "Edge.h"

namespace brite {

int Edge::edge_count = 0;

Edge::Edge(BriteNode* s, BriteNode* d) 
{

  assert(s != NULL && d != NULL);
  src = s;
  dst = d;
  color = BLACK;
  conf = NULL;
  id = edge_count;
  edge_count += 1;
  directed = false; /* Undirected by default */

}

Edge::~Edge() {

  delete src;
  delete dst;
  delete conf;

}

ASEdgeConf::ASEdgeConf() {

  as_edge_type = AS_NONE;
  SetBW(0.0);
  SetCost(0.0);

}



RouterEdgeConf::RouterEdgeConf(double len) {

  rt_edge_type = RT_NONE;
  length = len;
  SetBW(0.0);
  delay = 1000.0 * (1000.0 * length)/SPEED_OF_LIGHT;
  SetCost(0.0);

}

/* Euclidean distance between two vertices */
double Edge::Length() {

  double dx, dy;
  double foo;

  dx = (double) src->GetNodeInfo()->GetCoordX() -  (double)dst->GetNodeInfo()->GetCoordX();
  dy = (double) src->GetNodeInfo()->GetCoordY() -  (double)dst->GetNodeInfo()->GetCoordY();
  foo = (dx*dx) + (dy*dy);
  return sqrt(foo);

}

} // namespace brite
