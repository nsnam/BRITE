/*
 * Copyright 2001, Trustees of Boston University.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Author:     Alberto Medina
 *              Anukool Lakhina
 *  Title:     BRITE: Boston university Representative Topology gEnerator
 *  Revision:  2.0         4/02/2001
 */
/****************************************************************************/
/*                                                                          */
/*  Modified lightly to easily interface with ns-3                          */
/*  Author:     Josh Pelkey <jpelkey@gatech.edu>                            */
/*  Date: 3/02/2011                                                         */
/****************************************************************************/
/*  Relicensed to GPLv2, with permission of the original authors.           */
/*  Date: 04/20/2024                                                        */
/****************************************************************************/

#include "Graph.h"

using namespace std;
namespace brite {

Graph::Graph(int n) : nodes(n), adjList(n), incList(n) {

  assert(n > 0);
  numNodes = n;
  numEdges = 0;

}


void Graph::AddNode(BriteNode* node, int index)
{

  assert(index >= 0 && index < numNodes); 

  try {

    nodes[index] = node;
    
  }

  catch (bad_alloc) {

    cout << "Graph::AddNode(): could not allocate node...\n" << flush;
    exit(0);
  
  }
}

void Graph::AddEdge(Edge *edge) {

  assert(edge != NULL);
  edges.insert(edges.end(), edge);
  numEdges++;

}

void Graph::AddAdjListNode(int n1, int n2) {

  adjList[n1].insert(adjList[n1].begin(), n2);

}

void Graph::AddIncListNode(Edge* edge) {

  assert(edge != NULL);
  int n1 = edge->GetSrc()->GetId(); 
  int n2 = edge->GetDst()->GetId(); 

  incList[n1].insert(incList[n1].begin(), edge);
  incList[n2].insert(incList[n2].begin(), edge);

}

BriteNode* Graph::GetNodePtr(int index) {

  assert(index >= 0 && index < numNodes);
  return nodes[index];

};

int Graph::GetNumNodes() { 

  return numNodes; 

}

int Graph::GetNumEdges() { 

  return numEdges; 

}

void Graph::SetNumNodes(int n) {
    
    assert(n > 0);
    numNodes = n; 

}

bool Graph::AdjListFind(int n1, int n2) {

  list<int>::iterator li;
  for (li = adjList[n1].begin(); li != adjList[n1].end(); li++) {
    if (*li == n2) {
      return 1;
    }
  }
  
  return 0;

}

void Graph::DFS(vector<Color>& color, vector<int>& pi, int u) {

  int v;

  color[u] = GRAY;
  list<int>::iterator li;

  for (li = adjList[u].begin(); li != adjList[u].end(); li++) {
    v = *li;
    if (color[v] == WHITE) {
      pi[v] = u;
      DFS(color, pi, v);
    }
  }

  color[u] = BLACK;

}
  
} // namespace brite
