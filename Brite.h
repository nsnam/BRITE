/*
 * Copyright 2001, Trustees of Boston University.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Author:     Alberto Medina
 *              Anukool Lakhina
 *  Title:     BRITE: Boston university Representative Topology gEnerator
 *  Revision:  2.0         4/02/2001
 */
/****************************************************************************/
/*  Modified lightly to easily interface with ns-3                          */
/*  Author:     Josh Pelkey <jpelkey@gatech.edu>                            */
/*  Date: 3/02/2011                                                         */
/****************************************************************************/
/*  Relicensed to GPLv2, with permission of the original authors.           */
/*  Date: 04/20/2024                                                        */
/****************************************************************************/
#ifndef BRITE_H
#define BRITE_H

#include <iostream>
#include <string>

#include "Parser.h"
#include "Models/Model.h"
#include "Models/RouterWaxmanModel.h"
#include "Models/RouterBarabasiAlbertModel.h"
#include "Models/ASWaxmanModel.h"
#include "Models/ASBarabasiAlbertModel.h"
#include "Models/ImportedFileModel.h"
#include "Models/TopDownHierModel.h"
#include "Models/BottomUpHierModel.h"
#include "Models/ImportedFileModel.h"
#include "Topology.h"

namespace brite {

class Brite {

public:

  Brite (std::string, std::string, std::string);
  ~Brite ();

  void InitSeeds(char *);
  void OutputSeeds(char *);
  void ImportFile (std::string, std::string, std::string);
  Topology* GetTopology (void);

private:
  Topology* m_topology;
};

} // namespace brite

#endif /* BRITE_H */



