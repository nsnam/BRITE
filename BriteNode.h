/*
 * Copyright 2001, Trustees of Boston University.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Author:     Alberto Medina
 *              Anukool Lakhina
 *  Title:     BRITE: Boston university Representative Topology gEnerator
 *  Revision:  2.0         4/02/2001
 */
/****************************************************************************/
/*  Modified lightly to easily interface with ns-3                          */
/*  Author:     Josh Pelkey <jpelkey@gatech.edu>                            */
/*  Date: 3/02/2011                                                         */
/****************************************************************************/
/*  Relicensed to GPLv2, with permission of the original authors.           */
/*  Date: 04/20/2024                                                        */
/****************************************************************************/
#ifndef BRITENODE_H
#define BRITENODE_H

#include "Util.h"
#include "Graph.h"
#include "Topology.h"

namespace brite {

class Topology;
class NodeConf;
class Graph;

class BriteNode {

 public:

  BriteNode(int i);
  BriteNode(NodeConf* info);

  int GetId() { return nodeId;  }
  int GetAddr() { return nodeAddr; }
  int GetInDegree() { return inDegree; }
  int GetOutDegree() { return outDegree; }
  Color GetColor() { return nodeColor; }
  NodeConf* GetNodeInfo() { return nodeInfo; }
  
  void SetId(int id) { nodeId = id;  }
  void SetAddr(int addr) { nodeAddr = addr; }
  void SetInDegree(int degree) { inDegree = degree; }
  void SetOutDegree(int degree) { outDegree = degree; }
  void SetColor(Color col) { nodeColor = col; }
  void SetNodeInfo(NodeConf*  info) { nodeInfo = info; }

 private:

  int nodeId;
  int nodeAddr;
  int inDegree;
  int outDegree;
  Color nodeColor;
  NodeConf* nodeInfo;

};

// to derive from it specific node types
class NodeConf {

 public:

  enum NodeType { AS_NODE = 1, RT_NODE = 2 };
  double GetCost() { return cost; }
  void SetCost(double c) { cost = c; }
  NodeType GetNodeType() { return type; }
  void SetNodeType(NodeType t) { type = t; }
  double GetCoordX() { return x; }
  double GetCoordY() { return y; }
  double GetCoordZ() { return z; }
  void SetCoord(double xval, double yval, double zval) {
    x = xval;
    y = yval;
    z = zval;
  }

 private:
  
  double cost;
  NodeType type;
  double x;
  double y;
  double z;

};


class RouterNodeConf : public NodeConf {

 public:

  RouterNodeConf();
  enum RouterNodeType { RT_NONE, RT_LEAF, RT_BORDER, RT_STUB, RT_BACKBONE };
  RouterNodeType GetRouterType() { return rttype; }
  void SetRouterType(RouterNodeType t) { rttype = t; }
  int GetASId() { return ASid; }
  void SetASId(int i) { ASid = i; }

 private:
  
  RouterNodeType rttype;
  int ASid;

};

class ASNodeConf : public NodeConf {

 public:

  enum ASNodeType {AS_NONE, AS_LEAF, AS_STUB, AS_BORDER, AS_BACKBONE};
  ASNodeConf();
  ~ASNodeConf();
  Topology* GetTopology() { return t; }
  void SetTopology(Topology* top, int asid);
  ASNodeType GetASType() { return astype; }
  void SetASType(ASNodeType t) { astype = t; }
  int GetASId() { return ASid; }
  void SetASId(int i) { ASid = i; }

 private:

  Topology* t;
  ASNodeType astype;
  int ASid;

};

} // namespace brite

#endif /* BRITENODE_H */
